class RPNCalculator
  # TODO: your code goes here!
  attr_accessor :value,:acc

  def initialize
    @value = 0
    @acc = []
  end

  def push (input)
    @acc.push(input)
  end

  def plus
    if @acc.length == 0
      raise "calculator is empty"
    end
    val =  @acc[-2] + @acc[-1]
    @acc.pop
    @acc.pop
    @acc.push(val)
    @value = val
  end

  def minus
    if @acc.length == 0
      raise "calculator is empty"
    end
    val =  @acc[-2] - @acc[-1]
    @acc.pop
    @acc.pop
    @acc.push(val)
    @value = val
  end


  def divide
    if @acc.length == 0
      raise "calculator is empty"
    end
    val =  @acc[-2] * 1.0 / @acc[-1] * 1.0
    @acc.pop
    @acc.pop
    @acc.push(val)
    @value = val
  end

  def times
    if @acc.length == 0
      raise "calculator is empty"
    end
    val =  @acc[-2] * @acc[-1]
    @acc.pop
    @acc.pop
    @acc.push(val)
    @value = val
  end

  def tokens(input)
    input_arr =  input.split(" ")

    input_arr.each_with_index do |el,idx|
      if "*+-/".include?(el)
        input_arr[idx] = el.to_sym
      else
        input_arr[idx] = el.to_i
      end
    end
    input_arr
  end

  def evaluate(input)
    input_arr = tokens(input)
    mycalc = RPNCalculator.new
    input_arr.each_with_index do |el,idx|
      if el.is_a? Numeric
        mycalc.push(el)
      elsif el == :+
        mycalc.plus
      elsif el == :-
        mycalc.minus
      elsif el == :/
        mycalc.divide
      elsif el == :*
        mycalc.times
      end
    end
    mycalc.value
  end


end
